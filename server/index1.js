const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const multer = require('multer');
const routes = require('./routes/index.js');
const md5 = require('md5');

var port = process.env.PORT || 3000;

var Storage = multer.diskStorage({
    
    destination: function(req, file, callback) {
        callback(null, "./public/uploads");
    },
    filename: function(req, file, callback) {
        //callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
        callback(null, md5( Date.now())+ file.originalname);
    }
});
var upload = multer({
    storage: Storage
}).array("files", 12);

app.use(bodyParser.json());
app.use(function (req, res, next) {
    req.upload = upload;
    next();
});

app.use('/api/v1', routes);

app.listen(port,() => {
    console.log('server port '+port);
});