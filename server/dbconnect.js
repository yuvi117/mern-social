const mongoose = require('mongoose');
let dev_db_url = 'mongodb://localhost:27017/mern_social';
const mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });
mongoose.Promise = global.Promise;

module.exports  =  mongoose.connection;