const express = require('express');
const router = express.Router();

const auth = require('../middleware/auth');
const userController = require('../controllers/user.controller');
const chatController = require('../controllers/chat.controller');
const authController = require('../controllers/auth.controller');
const manageRequests = require('../controllers/manageRequests.controller');

router.get('/', auth, userController.landing);
router.post('/users', auth, userController.users);
router.get('/user/:id', auth, userController.user);
router.post('/my-friends', auth, userController.myFriends);
router.post('/my-requests', auth, userController.myRequests);
router.post('/add-user', userController.addUser);

router.post('/login', authController.login);
router.post('/logout', auth, authController.logout);

router.post('/send-request', auth, manageRequests.sendRequest);
router.post('/manage-request', auth, manageRequests.manageRequest);

router.get('/chat/:id', auth, chatController.getChat);
router.get('/inbox', auth, chatController.getInbox);
router.post('/send-message', auth, chatController.sendMessage);