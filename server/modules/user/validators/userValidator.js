const Joi = require('@hapi/joi');
const { responses } = require('../../../util');

const userValidations = {
  addUser: (req, res, next) => {
    const schema = Joi.object({
      name: Joi.string().required(),
      email: Joi.string().required().email({ minDomainSegments: 2, tlds: { allow: ["com", "net", "in"]} }),
      password: Joi.string().required().min(6).max(16),
      confirm_password: Joi.any().required().equal(Joi.ref('password')),
      phone: Joi.number().required().positive().integer().min(1000000000).max(9999999999),
      pincode: Joi.number().required().positive().integer().min(11111).max(999999),
    });

    const validateBody = schema.validate(req.body);
    if (validateBody.error) {
      //console.log('validateBody error', validateBody.error)
      const errorMessage = validateBody.error.details[0].message;
      return responses.sendError(res, "", {}, errorMessage, 'PARAMETER_MISSING');
    }
    req.body = validateBody.value;
    next();
  },
}

module.exports = userValidations;