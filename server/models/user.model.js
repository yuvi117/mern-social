const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UserSchema = new Schema({
    name: {type: String, required: true, max:100},
    role:{type: String, enum: ['user', 'admin', 'developer'], default:'user'},
    email: {type: String, required: true, max:255, unique: true},
    password: {type: String, required: true, max:255},
    phone: {type: Number, required: true},
    pincode: {type: Number, required: true},
    isVerified: {type: Boolean, required: true, default:0},
    status:{type: String, enum: [1,2], default:1},
    deletedAt: {type: Date},
   // createdAt: {type: Date, required: true},
    //updatedAt: {type: Date, required: true},
},
{
  timestamps: true
});

module.exports = mongoose.model('User', UserSchema);