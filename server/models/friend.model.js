const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const User = require('../models/user.model');


let FriendSchema = new Schema({
    senderId: {type: Schema.Types.ObjectId, ref: 'User'},
    receiverId: {type: Schema.Types.ObjectId, ref: 'User'},
    status:{type: String, enum: ['requested','rejected', 'accepted'], default:'requested'},
    isBlocked:{type: Boolean, required: true, default:0}
},
{
  timestamps: true
});

module.exports = mongoose.model('Friend', FriendSchema);