const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ChatRoomSchema = new Schema({
    userOne: {type: Schema.Types.ObjectId, ref: 'User'},
    userTwo: {type: Schema.Types.ObjectId, ref: 'User'},
    isBlocked:{type: Boolean, required: true, default:0}
},
{
  timestamps: true
});

module.exports = mongoose.model('ChatRoom', ChatRoomSchema);