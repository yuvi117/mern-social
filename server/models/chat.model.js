const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Chat = new Schema({
    chatRoomId: {type: Schema.Types.ObjectId, ref: 'chatRoom'},
    sentBy: {type: Schema.Types.ObjectId, ref: 'User'},
    receivedBy: {type: Schema.Types.ObjectId, ref: 'User'},
    message:{type: String, required: true},
    messageStatus:{type: String, enum: ['sent', 'delivered', 'received'], default:'sent'},
    messageType:{type: String, enum: ['text', 'image', 'video'], default:'text'},
    isBlocked:{type: Boolean, required: true, default:0},
    seenAt: {type: Date},
    deletedBySender: {type: Date},
    deletedByReceiver: {type: Date}
},
{
  timestamps: true
});

module.exports = mongoose.model('Chat', Chat);