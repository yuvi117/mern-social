const Friend = require('../models/friend.model');

exports.sendRequest = function(req, res, next){
    
    let friend = new Friend({
        receiverId: req.body._id,
        status: req.body.status,
        senderId: req.body.senderId
    });

    friend.save(function(err){
        if (err) return next(err);

        return res.send({status:200, message:"Friend request sent successfully!"});
    })
};

exports.manageRequest = function(req, res, next){
    
    if ( req.body.status == 'unfriend') {
        Friend.deleteOne({'_id': req.body._id, 'status' : 'accepted'}, function (err, user) {
            if (err) return next(err);
            return res.send({status:200, message:"Unfriend successfully!"});
        });
    } else {        
        Friend.updateOne({'_id': req.body._id, 'status' : 'requested'}, {$set:{'status' : req.body.status}}, function (err, user) {
            if (err) return next(err);
            return res.send({status:200, message:"Request accepted successfully!", data:user});
        });
    }
};