const Chat = require('../models/chat.model');
const ChatRoom = require('../models/chatRoom.model');
const ObjectId = require('mongodb').ObjectId;

exports.sendMessage = function(req, res, next){

    let o_id = new ObjectId(req.body.receiverId);
    let my_id = new ObjectId(req.currUser.userId);

    ChatRoom.findOne({ $or: [ { 'userOne': ObjectId(my_id), 'userTwo': ObjectId(o_id), 'isBlocked': 0 }, { 'userOne': ObjectId(o_id), 'userTwo': ObjectId(my_id), 'isBlocked' : 0 } ]}, function (err, room) {
        if (err) return next(err);

        if(!room){
            room = new ChatRoom({
                userOne: req.body.receiverId,
                userTwo: req.currUser.userId
            });
            room.save(function(err){
                if (err) return next(err)
            });
        }

        let message = new Chat({
            chatRoomId: room._id,
            sentBy: req.currUser.userId,
            receivedBy: req.body.receiverId,
            message: req.body.message,
        });
        message.save(function(err){
            if (err) return next(err);
        });
        return res.send({status:200, message:"Message sent successfully", data:message});
    });
};

exports.getChat = function (req, res, next) {

    if ( req.params.id ) {

        let o_id = new ObjectId(req.params.id);
        let my_id = new ObjectId(req.currUser.userId);
        Chat.find({ $or: [ { 'sentBy': ObjectId(o_id), 'receivedBy': ObjectId(my_id) }, { 'receivedBy': ObjectId(o_id), 'sentBy': ObjectId(my_id)} ]}, function (err, chat) {
            if (err) return next(err);
            return res.send({status:200, message:"All messages", data:chat});
        }).populate('sentBy', ['_id', 'name']).populate('receivedBy', ['_id', 'name'])//.sort( { updatedAt: -1 } );
    } else {
        return res.send({status:400, message:"User Id required"});
    }
};

exports.getInbox = function (req, res, next) {

    if ( req.currUser.userId ) {
        let my_id = new ObjectId(req.currUser.userId);
        ChatRoom.find({ $or: [ { 'userOne': ObjectId(my_id), 'isBlocked': 0 }, { 'userTwo': ObjectId(my_id), 'isBlocked' : 0 } ]}, function (err, user) {
            if (err) return next(err);
            return res.send({status:200, message:"My Inbox", data:user});
        }).populate('userOne', ['_id', 'name']).populate('userTwo', ['_id', 'name']);
    } else {
        return res.send({status:400, message:"User Id required"});
    }
};