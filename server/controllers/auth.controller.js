const User = require('../models/user.model');
const config = require('../config.js');
const JWT = require('jsonwebtoken');

exports.login = function(req, res){

    let { email, password } = req.body;
    if (email && password) {
        User.findOne({'email': req.body.email, 'password': req.body.password}, function (err, user) {
            if (err) return next(err);

            if(!user){
                res.send({status:400, message:"Incorrect Email/Password!"});
		        return res.end();
            }
            // let currectPwd = Bcrypt.compareSync(userPassword, user.userPassword)
            const { secret, refreshSecret } = config;

            const payload = {
                userId: user._id,
                userName: user.name,
                userEmail: user.email,
            };

            try {
                const signOptions = {
                    issuer: 'sunil',
                    expiresIn: '1m'
                };
                
                payload.creationDateTime = Date.now();
                user = user.toObject();
                user.token = JWT.sign(payload, secret, signOptions);

                user.refreshToken = JWT.sign(payload, refreshSecret, 
                    {
                        expiresIn: "1d",
                    }
                );

            } catch (error) {
                console.log('toekerrn', error);
            }
            res.send({status:200, message:"You have logged in successfully!", data:user});
            return res.end();
        });
    } else {
        res.send({status:400, message:"Please enter Email and Password!"});
        return res.end();
    }
};

exports.logout = function(req, res){
    try {
        const accessToken = req.headers.access_token;
        const user = req.user;
        const opts = {
            accessToken,
            user
        };
        //await commonFunctions.validateUser(opts);
        //const userTokens = user.accessTokens;
        // let accessTokens;
        // if (userTokens && userTokens.length > 0) {
        //     accessTokens = userTokens.filter((token) => {
        //         return token != accessToken;
        //     });
        // }
        // await User.findByIdAndUpdate(user._id, {
        //     $set: {
        //         accessTokens: accessTokens
        //     }
        // });
        res.send({status:200, message:"loggedout successfully"});
        res.end();
    } catch (error) {
        console.log(error);
        res.send({status:400, message:"Please try again!"});
        res.end();
    }
};

exports.refreshToken = async function(req, res){
    try {
        const { refreshToken } = req.body;

        //send error if no refreshToken is sent
        if (!refreshToken) {
          return res.status(403).send({ error: "Access denied,token missing!" });
        } else {

         // const tokenDoc = await Token.findOne({ token: refreshToken });
          //send error if no token found:
          //if (!tokenDoc) {
            //return res.status(401).json({ error: "Token expired!" });
          //} else {
            //extract payload from refresh token and generate a new access token and send it
            const { secret, refreshSecret } = config;
            const payload = JWT.verify(refreshToken, refreshSecret);
            ['exp', 'iat', 'creationDateTime'].forEach(e => delete payload[e]);
            // console.log('payload', payload);
            const accessToken = JWT.sign(payload, secret, {
                issuer: 'sunil',
                expiresIn: "1m",
            });
            return res.status(200).json({ accessToken });
          //}
        } 
    } catch (error) {
        console.log(error);
        res.send({status:400, message:"Please try again!"});
        res.end();
    }
};