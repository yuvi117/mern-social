const User = require('../models/user.model');
const Friend = require('../models/friend.model');
const ObjectId = require('mongodb').ObjectId;

exports.landing = function(req, res, next){
    User.findOne({}, function (err, user) {
        if (err) return next(err);
        res.send({status:200, message:"All Users", data:user});
    });
};

exports.users = function (req, res, next) {

    let rr = {};
    if ( req.query.filter ) {

        rr = req.query.filter.split(",or,").map((column) => {
            return column.split(",contains,").map((v,s) => v)
        }).reduce(function(obj, str) {
          let strParts = {...str};
          if (strParts[0] && strParts[1]) { 
            obj[strParts[0].replace(/\s+/g, '')] = '^'+strParts[1].trim();
          }
          return obj;
        }, {});

    }
//    console.log('dd', Object.keys(rr).length)
    console.log('dd1', (rr))
    // {'name': {'$regex': 's'}}

    User.find({_id: { $ne: req.query._id }, ...rr }).skip(parseInt(req.query.skip)).limit(parseInt(req.query.take)).populate('User', ['_id', 'name'])
    .exec(function(err, users) {    
        if (err) return next(err);
        User.find({_id: { $ne: req.query._id }}).countDocuments().exec(function(err, count) {
           res.send({status:200, message:"All Users", data:users, totalCount: count});
        })
    });
};

exports.user = function (req, res, next) {

    if ( req.params.id ) {
        User.findOne({ _id: req.params.id }, function (err, user) {
            if (err) return next(err);
            return res.send({status:200, message:"User profile", data:user});
        });
    } else {
        return res.send({status:400, message:"User Id required"});
    }
};

exports.myFriends = function(req, res, next){
    if ( req.currUser.userId){
        let o_id = new ObjectId(req.currUser.userId);
        Friend.find( { $or: [ { 'senderId': ObjectId(o_id), 'status' : 'accepted' }, { 'receiverId': ObjectId(o_id), 'status' : 'accepted' } ]}, function (err, user) {
            if (err) return next(err);
            return res.send({status:200, message:"My friends", data:user});
        }).populate('receiverId', ['_id', 'name']).populate('senderId', ['_id', 'name']);
    } else {
        return res.send({status:400, message:"User Id Missing"});
    }
};

exports.myRequests = function(req, res, next){
    let o_id = new ObjectId(req.body._id);
    Friend.find({'receiverId': ObjectId(o_id), 'status' : 'requested'}, function (err, user) {
        if (err) return next(err);
        res.send({status:200, message:"My friends", data:user});
    }).populate('receiverId', ['_id', 'name']).populate('senderId', ['_id', 'name']);
};

exports.addUser = function(req, res, next){

    let user = new User({
        name: req.body.name,
        role: req.body.role,
        email: req.body.email,
        password: req.body.password,
        phone: req.body.phone,
        pincode: req.body.pincode,
    });
    user.save(function (err) {
        if (err) return next(err);
        res.send({status:200, message:'User Created successfully', data:user})
        return res.end();
    });
};

exports.deleteUser = function (req, res, next) {
console.log('in delete', req.params)
    if ( req.params.id ) {
        // User.findOne({ _id: req.params.id }, function (err, user) {
        //     if (err) return next(err);
        //     return res.send({status:200, message:"User profile", data:user});
        // });
        return res.send({status:200, message:"User profile"});
    } else {
        return res.send({status:400, message:"User Id required"});
    }
};