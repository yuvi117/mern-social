require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const apiRoutes = require('./routes/index.js');
const port = process.env.PORT || 4000;
const cors = require('cors');

// Set up mongoose connection
const db = require('./dbconnect');

db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(cors());
app.use('/api/v1',  apiRoutes);

app.listen(port, () => {
    console.log("Server is listening on port "+port);
});