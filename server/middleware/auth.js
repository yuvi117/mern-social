const jwt = require('jsonwebtoken');
const config = require('../config.js');
 
module.exports = function (req, res, next) {

    let token = req.headers['x-auth-token'] || req.headers['authorization'];

    if (!token) {
        return res.status(403).send({"status": 403, "message": 'Access denied. No JWT Token provided.'});
    }

    if (token.startsWith('Bearer ')) {

        token = token.slice(7, token.length);
    }  

    jwt.verify(token, config.secret, function (err, currUser) {
        if (err) {
            res.status(401).send({"status": 401, "message": 'Invalid JWT Token!'});
        } else {
            req.currUser = currUser;
            next();
        }
    });
}