const mongoose = require('mongoose');

let db = mongoose.connect(process.env.MONGODB_URL, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true }, function (err) {

    if (err) throw err;
        console.log('Successfully connected with mongoDB');
});
module.exports = {
    db
};