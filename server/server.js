require('dotenv').config();
const app = require('express')();
const server  = require('http').createServer(app);
const io      = require('socket.io').listen(server);
const Redis   = require('ioredis');
const redisClient = Redis.createClient();
const PortNumber = process.env.SOCKET_IO_PortNumber || 8002;
const db = require("./dbconnect");
const jwt = require('jsonwebtoken');
const config = require('./config.js');
const Chat = require('./models/chat.model');
const ChatRoom = require('./models/chatRoom.model');
const ObjectId = require('mongodb').ObjectId;

db.then(dbc  =>  {
    // console.log("connected with mongodb");
});

server.listen(PortNumber, function(){
    console.log("Websockets listening on localhost:" + PortNumber)
});

io.on('connection', async (socket) => {

  let token = socket.handshake.query.accessToken;
  if (!token) {
    return socket.emit('socketError', {status:401, message:'JWT token required'});
  }

  let data = await checkVerifyToken(token);
  if ( data.status == 401 ) {
    return socket.emit('socketError', {status:401, message:'unauth'});
  }

  let foundUser = data.user;
  await redisClient.set(foundUser.userId,socket.id);
  console.log(foundUser.userName + " connected");

  socket.on('disconnect', async () => {

    let keys = await redisClient.keys('*');
    let userId="";

    for (let i=0; i<keys.length; i++) {
      let getValue = await redisClient.get(keys[i]);
      if ( getValue == socket.id ) {
        userId = keys[i];
        break;
      }
    }
    console.log(userId + " disconnected");
  });

  socket.on('send', chat => {

    let o_id = new ObjectId(chat.receivedBy);
    let my_id = new ObjectId(chat.sentBy);

    ChatRoom.findOne({ $or: [ { 'userOne': ObjectId(my_id), 'userTwo': ObjectId(o_id), 'isBlocked': 0 }, { 'userOne': ObjectId(o_id), 'userTwo': ObjectId(my_id), 'isBlocked' : 0 } ]}, async (err, room) => {
      if (err) console.log('socket chatroom erro', err);

      if(!room){
          room = new ChatRoom({
              userOne: o_id,
              userTwo: my_id
          });
          room.save(function(err){
              if (err) console.log('socket chatroom insert err', err)
          });
      }

      let message = new Chat({
          chatRoomId: room._id,
          sentBy: my_id,
          receivedBy: o_id,
          message: chat.message,
      });
      message.save(function(err){
          if (err) console.log('chat insert err', err);
      });
      
      let socketId = await redisClient.get(o_id);
      io.to(socketId).emit('messageReceived', message);
      let sentSocketId = await redisClient.get(my_id);
      io.to(sentSocketId).emit('messageSent', message);
    });
  })
});

const checkVerifyToken = async (token)  => {
  return new Promise((resolve, reject) => {
    jwt.verify(token, config.secret, function (err, currUser) {
      if (err)
          resolve( {"status": 401, "message": 'Invalid JWT'});
      else
          resolve( {"status": 200, "message": 'Valid JWT', user:currUser});
    });
  });
}