export default function validate(values) {
  let errors = {};
  if (values.name && !/^[a-zA-Z\s]{2,20}$/.test(values.name)) {
    errors.name = 'Name must be 3 to 25 characters and started with alphabets only';
  }
  if (values.email && !/\S+@\S+\.\S+/.test(values.email)) {
    errors.email = 'Email address is invalid';
  }
  if (values.password && !/^.{6,16}$/.test(values.password)) {
    errors.password = 'Password must be 6 or more characters';
  }
  if (values.confirm_password && (values.password !== values.confirm_password)){
    errors.confirm_password = 'Password & Confirm Password must be matched';
  }
  if (values.phone && !/^[0-9]{10}$/.test(values.phone)) {
    errors.phone = 'Phone must be number between 5 to 6 characters';
  }
  if (values.pincode && !/^[0-9]{5,6}$/.test(values.pincode)) {
    errors.pincode = 'Pincode must be number between 5 to 6 characters';
  }
  return errors;
};
  