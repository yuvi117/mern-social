import React, {useState, useEffect} from 'react';
import { Container, Row, Col } from "react-bootstrap";
import Button from '@material-ui/core/Button';

const Requests = (props) => {
    
    const [users, setUsers] = useState(false);
    const [message, setMessage] = useState(false);

    useEffect( () => {
        usersData();
    }, []);

    async function usersData(){
            
        let res = await fetch(`${process.env.REACT_APP_SERVER_URL}/api/v1/my-requests`, {
            method:'post',
            headers :{
                'Content-Type': 'application/json',
                'Authorization':'Bearer ' + localStorage.getItem('jwtToken')
            },
            body: JSON.stringify({_id:localStorage.getItem('userId')})

        });
        let data = await res.json();
        if (data.status === 200) {
            //console.log('sds', data.data);
            setUsers(data.data);
        } else if (data.status === 401) {
            setMessage(data.message);
        }
     };

    async function handleRequest(id, status){
        
        let res = await fetch(`${process.env.REACT_APP_SERVER_URL}/api/v1/manage-request`, {
            method: 'POST',
            headers :{
                'Content-Type': 'application/json',
                'Authorization' : 'Bearer '+ localStorage.getItem('jwtToken')
            },
            body: JSON.stringify({_id:id, status:status})
        });
        
        let data = await res.json();

        if (data.status === 200) {
            setMessage(data.message);
            usersData();
            //props.history.push("/my-requests");
        } else if (data.status === 400) {
            setMessage(data.message);
        }
    }

    if ( users.length === 0) {
        return <>{ 'You don\'t have any pending request at the moment.'}</>;
    }

    return <>
        <Container>
        {message}
        {Object.values(users).map((friend, i) =>
            <Row>
                <Col md={4}>
                    {friend.senderId._id === localStorage.getItem('userId') ? friend.receiverId.name : friend.senderId.name}
                </Col>
                <Col md={8} sm={8} lg={8}>
                    <Button variant="contained" color="primary" onClick={() => handleRequest(friend._id, 'accepted')} >Accept Request</Button>
                    <Button variant="contained" color="secondary" onClick={() => handleRequest(friend._id, 'rejected')} >Reject Request</Button>
                </Col>
            </Row>
        )}
        </Container>
    </>;
}

export default Requests;