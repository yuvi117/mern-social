import React, {useState, useEffect} from 'react';
import { Row, Col, Button } from "react-bootstrap";
// import Button from '@material-ui/core/Button';
import { NavLink } from "react-router-dom";
// import ODataStore from 'devextreme/data/odata/store';
import CustomStore from 'devextreme/data/custom_store';
// import DiscountCell from './DiscountCell.jsx';
// import '@devexpress/dx-react-grid-bootstrap4/dist/dx-react-grid-bootstrap4.css';
import DataGrid, {
    Column,
    Pager,
    Paging,
    SearchPanel,
    Editing
  } from 'devextreme-react/data-grid';
import { API_URL, HEADER } from '../../constants/config';

const dataSourceOptions = new CustomStore({
    load: async (loadOptions) => {
        return await fetch(`${API_URL}/my-friends`, {
            method:'POST',
            headers : HEADER,
            body: JSON.stringify({_id:localStorage.getItem('userId')})
        })
          .then(response => response.json())
          .then((data) => {
            //   console.log('h', data)
              return data.data;
            // return {
            //     Customer: 'data.data',
            //   totalCount: data.totalCount,
            //   summary: data.summary,
            //   updatedAt: data.updatedAt
            // };
          })
          .catch(() => { throw 'Data Loading Error'; });
      }
});

const Content = (props) => {
    
    const [users, setUsers] = useState(false);
    const [message, setMessage] = useState(false);
    const [collapsed, setCollapsed] = useState(false);

    useEffect( () => {
        usersData();
    }, []);

    // this.onContentReady = this.onContentReady.bind(this);

    const onContentReady = (e) => {
        if (!collapsed) {
            e.component.expandRow(['EnviroCare']);
            setCollapsed(true);
        }
    }

    async function usersData(){
            
        let res = await fetch(`${API_URL}/my-friends`, {
            method:'POST',
            headers : HEADER,
            body: JSON.stringify({_id:localStorage.getItem('userId')})
        });
        let data = await res.json();
        if (data.status === 200) {
            // console.log('sds', data.data);
            setUsers(data.data);
        } else if (data.status === 401) {
            setMessage(data.message);
        }
    };

    async function handleRequest(id, status){
        let res = await fetch(`${API_URL}/manage-request`, {
            method: 'POST',
            headers : HEADER,
            body: JSON.stringify({_id:id, status:status})
        });
        
        let data = await res.json();

        if (data.status === 200) {
            setMessage(data.message);
            usersData();
        } else if (data.status === 400) {
            setMessage(data.message);
        }
    }

    const NameCell = (cellData) => {
        return cellData.data.senderId._id === localStorage.getItem('userId') ? cellData.data.receiverId.name : cellData.data.senderId.name;
    }

    if ( users.length === 0) {
        return <>{ 'You don\'t have any friend at the moment.'}</>;
    }
    return <>

        {message}
        <Row >
            <Col md={4} sm={4} lg={4}>
                {Object.values(users).map((friend, i) =>
                <Row key={i}>
                    <Col md={4} sm={4} lg={4}>
                        {friend.senderId._id === localStorage.getItem('userId') ? <NavLink exact to={`/friend/${friend.receiverId._id}`}>{friend.receiverId.name}</NavLink> : <NavLink exact to={`/friend/${friend.senderId._id}`}>{friend.senderId.name}</NavLink>}         
                    </Col>
                    <Col md={8} sm={8} lg={8}>
                        <Button onClick={() => handleRequest(friend._id, 'unfriend')} >Unfriend Request</Button>
                    </Col>
                </Row>
            )}
            </Col>
            <Col md={8} sm={8} lg={8}>
                <DataGrid
                    dataSource={dataSourceOptions}
                    allowColumnReordering={true}
                    showBorders={true}
                    onContentReady={onContentReady}
                >

                    <SearchPanel visible={true} highlightCaseSensitive={true} />
            
                    <Column
                    dataField="receiverId"
                    caption="Name"
                    dataType="string"
                    alignment="center"
                    cellRender={NameCell}
                    />
                    <Column
                    dataField="isBlocked"
                    caption="Is Blocked"
                    dataType="string"
                    />
                    <Column dataField="status" dataType="string" />
                    <Column dataField="updatedAt" dataType="datetime" />
                    <Editing
                    mode="row"
                    allowAdding={false}
                    allowDeleting={false}
                    allowUpdating={true}
                    />
                    <Pager allowedPageSizes={[10, 25, 50, 100]} showPageSizeSelector={true} />
                    <Paging defaultPageSize={10} />
                </DataGrid>
            </Col>
        </Row>
    </>;
}

export default Content;