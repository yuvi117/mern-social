import React , { useEffect } from 'react';
import { connect, useDispatch } from "react-redux";
import { Table } from "react-bootstrap";
import { getEmployees } from '../../store/actions/EmployeeAction';

const Employees = (props) => {

	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(getEmployees(true));
	}, [dispatch]);

	const renderEmployees = ( employees ) => {

		return Object.values(employees).map((employee, index) => {

			return <tr key={index}>
					<td>{employee.id}</td>
					<td>{employee.employee_name}</td>
					<td>{employee.employee_age}</td>
					<td>{employee.employee_salary}</td>
			</tr>
		});
	}

	return <>
		<div>
			{props.error.msg}
		</div>
		<Table responsive striped bordered hover size="sm">
			<thead>
				<tr>
				<th>#</th>
				<th>First Name</th>
				<th>Age</th>
				<th>Salary</th>
				</tr>
			</thead>
			<tbody>
				{renderEmployees(props.employees)}
			</tbody>
		</Table>
	</>;
}

const mapStateToProps = (state) => ({
	employees: state.employees,
	error:state.error
});

export default connect(mapStateToProps, {getEmployees})(Employees);