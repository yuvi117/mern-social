import React, {useState} from 'react';
import "../auth/login.styles.css";

const AddUser = (props) => {
  const [message, setMessage] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirm_password, setConfirmPassword] = useState("");
  const [phone, setPhone] = useState("");
  const [pincode, setPincode] = useState("");

  function validateForm(){
    return name.length > 0 && email.length > 0 && password.length > 0 && confirm_password.length > 0 && phone.length > 0 && pincode.length > 0;
  }

  async function handleSubmit(e){
    e.preventDefault();

    const user = {
      name:name,
      email:email,
      password:password,
      confirm_password:confirm_password,
      phone:phone,
      pincode:pincode,
    }

    let res = await fetch(`${process.env.REACT_APP_SERVER_URL}/api/v1/add-user`, {
      method:'POST',
      headers:{
          'Content-Type': 'application/json',
      }, 
      body: JSON.stringify(user),
    })

    let data = await res.json();

    if ( data.status === 200 ) {
      setMessage(data.message);
      //setState({name:"", email:"", password:"", phone:"", pincode:""});
      //document.getElementById("login-form").reset();
    } else if ( data.status === 400 ) {
      setMessage(data.message);
    }
  }

  return <>
    <div >
        {message}
        <form onSubmit={handleSubmit} id="login-form">
          <div className="row">
            <div className="col s12">
              <div className="row">
                <div className="input-field col m6">
                  <input type="text" value={name} onChange={e => setName(e.target.value)}/>
                  <label>Name</label>
                </div>
              
                <div className="input-field col m6">
                  <input type="text" value={email} onChange={e => setEmail(e.target.value)}/>
                  <label>Email</label>
                </div>
              </div>
              <div className="row">
                <div className="input-field col m6">
                <input type="password" value={password} onChange={e => setPassword(e.target.value)}/>
                  <label>Password</label>
                </div>

                <div className="input-field col m6">
                <input type="password" value={confirm_password} onChange={e=> setConfirmPassword(e.target.value)}/>
                  <label>Confirm Password</label>
                </div>
              </div>

              <div className="row">
                <div className="input-field col m6">
                <input type="text" value={phone} onChange={e => setPhone(e.target.value)}/>
                  <label>Phone</label>
                </div>
              
                <div className="input-field col m6">
                <input type="text" value={pincode} onChange={e => setPincode(e.target.value)}/>
                  <label>Pincode</label>
                </div>
              </div>
            </div>
          </div>
          <button className="btn waves-effect waves-light" disabled={!validateForm()} type="submit" >Submit <i className="material-icons right">send</i> </button>
        </form>
    </div>
    </>;
}

export default AddUser;