import React, {useState, useEffect} from 'react';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// import Table from '../partials/Table';
import { getUsers } from '../../store/actions/UserAction';
import ReactPaginate from 'react-paginate';

const Users = (props) => {
    
    const [message, setMessage] = useState(false);
    const [perPage] = useState(6);
    const [pageNo]  = useState(1);
    const [pageCount, setPageCount] = useState(10);
    
    const usersData = async (props) => {
        
        // let res = await fetch(`${process.env.REACT_APP_SERVER_URL}/api/v1/users`,{
            //     method:'post',
            //     headers :{
                //         'Content-Type': 'application/json',
                //         'Authorization' : 'Bearer '+ localStorage.getItem('jwtToken')
                //     },
        //     body: JSON.stringify({_id:localStorage.getItem('userId')})
        // });
        // let users = await res.json();
        
        // if ( users.status === 200 ) {
            //     setUsers(users.data);
        // } else if ( users.status === 400 ){
            //     setMessage(users.message);
            // }
        };
        
    useEffect( () => {
        props.getUsers({pageNo:pageNo, size:perPage});
        setPageCount(props.users.count);      
    }, [props.users.length]);

    const handlePageClick = (data) => {
        let selected = Math.ceil(data.selected +1);
        props.getUsers({pageNo:selected, size:perPage});
    }
    
    async function handleRequest(id){

        let requestData = {
            _id:id,
            status:'requested',
            'senderId': localStorage.getItem('userId')
        };
        
        let res = await fetch(`${process.env.REACT_APP_SERVER_URL}/api/v1/send-request`, {
            method: 'POST',
            headers :{
                'Content-Type': 'application/json',
                'Authorization' : 'Bearer '+ localStorage.getItem('jwtToken')
            },
            body: JSON.stringify(requestData)
        });
        
        let data = await res.json();

        if (data.status === 200) {
             usersData();
            setMessage(data.message);
        } else if (data.status === 400) {
            setMessage(data.message);
        }
    }

    // const columns = [
    //     {
    //         Header: 'Name',
    //         accessor: 'name',
    //     },
    //     {
    //         Header: 'Action',
    //         accessor: 'email',
    //     },
    // ];

    const columns = [{
        Header: 'Favorites',
        headerClassName: 'my-favorites-column-header-group',
        columns: [{
          Header: 'Color',
          accessor: 'name'
        }, {
          Header: 'Food',
          accessor: 'email'
        }]
      }]

    const renderUsers = (usersData) => {

        return usersData.map((user, i) =>
            <div className="row" key={i}>
                <div className="col m4">
                    {user.name}
                </div>
                <div className="col m4">                
                <button className="btn waves-effect waves-light" onClick={() => handleRequest(user._id)} type="submit">Add as friend</button>
                </div>
            </div>
        )
    }

    return <>
        {message}
        {/* {console.log('columns',columns)} */}
        {Object.keys(props.users).length !== 0 ? renderUsers(Object.values(props.users.data)) : ""}
        {/* <Table /> */}
        <ReactPaginate
          previousLabel={'previous'}
          nextLabel={'next'}
          breakLabel={'...'}
          breakClassName={'break-me'}
          pageCount={pageCount}
          marginPagesDisplayed={3}
          pageRangeDisplayed={5}
          onPageChange={handlePageClick}
          containerClassName={'pagination'}
          subContainerClassName={'pages pagination'}
          activeClassName={'active'}
        />
    </>;
}

Users.propTypes = {
    //getUsers: PropTypes.func.isRequired,
    // activeUser: PropTypes.object.isRequired,
    //users: PropTypes.object.isRequired,
    // error: PropTypes.string.isRequired
};

const mapStateToProps = state => ({
    error: state.error,
    //success: state.success,
    activeUser: state.users,
    users: state.users
});

export default connect(mapStateToProps, {
    getUsers
})(Users);