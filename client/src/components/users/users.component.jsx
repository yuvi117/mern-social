import React, { useState, useRef } from 'react';
import { Row, Col } from "react-bootstrap";
import { connect } from 'react-redux';
import { getUsers } from '../../store/actions/UserAction';
// import PropTypes from 'prop-types';
import DataGrid, {
  Column,
  Pager,
  Paging,
  SearchPanel,
  Editing,
  RemoteOperations,
  Form,
  Popup,
  FilterRow
} from 'devextreme-react/data-grid';
import { API_URL, HEADER } from '../../constants/config';
import CustomStore from 'devextreme/data/custom_store';
import swal from 'sweetalert';

const Users = (props) => {
  
  const sourceDataUseRef = useRef();
  const [message, setMessage] = useState(false);
  const isNotEmpty = (value) => value !== undefined && value !== null && value !== '';

  // const dataSource = new DataSource({
  //   load: async (loadOptions) => {
  //     return await fetch(`${API_URL}/users`, {
  //         method:'post',
  //         headers : HEADER,
  //         body: JSON.stringify({_id:localStorage.getItem('userId'), pageNo:Math.ceil(1), size:perPage})
  //     })
  //     .then(response => response.json())
  //     .then((data) => {
  //         console.log('h', data)
  //         //return data.data;
  //       return {
  //           data: data.data,
  //           totalCount: 21,              // if requireTotalCount = true
  //           summary: [3, 7, 9, 10], // total summary results
  //           groupCount: 20                // if requireGroupCount = true
  //       };
  //     })
  //     .catch(() => { throw 'Data Loading Error'; });
  //   }
  // });

  const dataSource = new CustomStore({
    load: async (loadOptions) => {
      loadOptions = {...loadOptions,_id:localStorage.getItem('userId')}
        let params = '?';
 
        [
          'filter',
          'group', 
          'groupSummary',
          'parentIds',
          'requireGroupCount',
          'requireTotalCount',
          'searchExpr',
          'searchOperation',
          'searchValue',
          'select',
          'sort',
          'skip',     
          'take',
          'totalSummary', 
          // 'userData',
          '_id'
        ].forEach(function(i) {
          if(i in loadOptions && isNotEmpty(loadOptions[i])) {
            params += `${i}=${(loadOptions[i])}&`;
          }
        });
        params = params.slice(0, -1);
        
        return fetch(`${API_URL}/users/${params}`, {
            headers : HEADER,
          })
        .then(handleErrors)
        .then(response => response.json())
        .then(response => {
          if ( response.status === 400) {
            setMessage(response.message);
            return false;
          }
          return {
            data: response.data,
            totalCount: response.totalCount,
          };
        })
        .catch(() => { throw 'Server error' });
    },
    update: function (key, values) {
      console.log('key value', key, values);
      swal('update successfully');
    },
    onUpdated: function (key, values) {
      swal('now successfully');
    },
    remove: (key) => {
      return fetch(`${API_URL}/user/${key._id}`, {
        method: "DELETE",
        headers : HEADER,
      })
      .then(handleErrors);
    },
  });

  const handleErrors = (response) => {
    if (!response.ok) {
      throw Error(response.statusText);
    }
    return response;
  }
  
  // const NameCell = (cellData) => {
  //   return cellData.data.senderId._id === localStorage.getItem('userId') ? cellData.data.receiverId.name : cellData.data.senderId.name;
  // }

  const usersData = async (props) => {
      
    // let res = await fetch(`${process.env.REACT_APP_SERVER_URL}/api/v1/users`,{
        //     method:'post',
        //     headers :{
            //         'Content-Type': 'application/json',
            //         'Authorization' : 'Bearer '+ localStorage.getItem('jwtToken')
            //     },
    //     body: JSON.stringify({_id:localStorage.getItem('userId')})
    // });
    // let users = await res.json();
    
    // if ( users.status === 200 ) {
        //     setUsers(users.data);
    // } else if ( users.status === 400 ){
        //     setMessage(users.message);
        // }
    };
      
  // useEffect( () => {
  //   props.getUsers({pageNo:pageNo, size:perPage});
  //   setPageCount(props.users.count);      
  // }, [props.users.length]);

  // const handlePageClick = (data) => {
  //   // let selected = Math.ceil(data.selected +1);
  //   // props.getUsers({pageNo:selected, size:perPage});
  // }
    
  // const handleRequest = async (id) => {

  //   let requestData = {
  //     _id:id,
  //     status:'requested',
  //     'senderId': localStorage.getItem('userId')
  //   };
    
  //   let res = await fetch(`${API_URL}/send-request`, {
  //     method: 'POST',
  //     headers : HEADER,
  //     body: JSON.stringify(requestData)
  //   });
    
  //   let data = await res.json();

  //   if (data.status === 200) {
  //     usersData();
  //     setMessage(data.message);
  //   } else if (data.status === 400) {
  //     setMessage(data.message);
  //   }
  // }

  // const renderUsers = (usersData) => {
  //   return usersData.map((user, i) =>
  //     <div className="row" key={i}>
  //       <div className="col m4">
  //           {user.name}
  //       </div>
  //       <div className="col m4">                
  //       <button className="btn waves-effect waves-light" onClick={() => handleRequest(user._id)} type="submit">Add as friend</button>
  //       </div>
  //     </div>
  //   )
  // }

  const setCustomValue = (e) => {
    const newSource = sourceDataUseRef.current.instance.getDataSource();
    newSource.reload();
    console.log('setCustomValue', newSource)
  }

  return <>
    {message}
    
    {/* {Object.keys(props.users).length !== 0 ? renderUsers(Object.values(props.users.data)) : ""}
    <ReactPaginate
      previousLabel={'previous'}
      nextLabel={'next'}
      breakLabel={'...'}
      breakClassName={'break-me'}
      pageCount={pageCount}
      marginPagesDisplayed={3}
      pageRangeDisplayed={5}
      onPageChange={handlePageClick}
      containerClassName={'pagination'}
      subContainerClassName={'pages pagination'}
      activeClassName={'active'}
    /> */}
    <Row >
      <Col md={12} sm={12} lg={12}>
        <DataGrid
          ref={sourceDataUseRef}
          dataSource={dataSource}
          allowColumnReordering={true}
          showBorders={true}
        >
        <FilterRow visible={true} />
        <RemoteOperations groupPaging={true} />
        <SearchPanel visible={true} highlightCaseSensitive={true} />

        <Column
          dataField="name"
          caption="Name"
          dataType="string"
          alignment="center"
        />
        <Column
          dataField="email"
          caption="Email"
          dataType="string"
        />
        <Column dataField="phone" dataType="string" allowFiltering={false} />
        <Column dataField="updatedAt" dataType="datetime" />
        <Column type="buttons" caption="Action" width={150}
          buttons={['edit', 'delete', {
            hint: 'Custom',
            icon: 'repeat',
            visible: true,
            onClick: setCustomValue
          }]} />
        <Editing
          useIcons={true}
          allowUpdating={true}
          allowDeleting={true}
          mode="popup">
          <Form labelLocation="top" />
          <Popup showTitle={true} title="Row in the editing state" />
        </Editing>

        <Pager allowedPageSizes={[10, 25, 50, 100]} showPageSizeSelector={true} />
        <Paging defaultPageSize={10} />
      </DataGrid>
    </Col>
    </Row>
  </>;
}

const mapStateToProps = state => ({
    error: state.error,
    //success: state.success,
    activeUser: state.users,
    users: state.users
});

export default connect(mapStateToProps, {
    getUsers
})(Users);