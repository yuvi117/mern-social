import React, {useState, useEffect, useCallback} from 'react';
import validate from '../validator/validateForm';
import "../auth/login.styles.css"; 

const AddUser = (props) => {
  const errorStyle = {
    color: 'red',
    fontSize: '13px',
  };
  const [message, setMessage] = useState("");
  const [values, setValues] = useState({});
  const [errors, setErrors] = useState({});
  const [isSubmitting, setIsSubmitting] = useState(false);
  
  function validateForm() {
    return (values.email && values.email.length > 0)
     && (values.password && values.password.length > 0)
     && (values.confirm_password && values.confirm_password.length > 0)
     && (values.phone && values.phone.length > 0)
     && (values.name && values.name.length > 0)
     && (values.pincode && values.pincode.length > 0);
  }

  const handleChange = (event) => {
    event.preventDefault();
    setValues({ ...values, [event.target.name]:event.target.value});
  }

  useEffect(() => {
    setErrors(validate(values));
  }, [values]);

  function handleSubmit(e){
    if (e) e.preventDefault();
    setErrors(validate(values));
    setIsSubmitting(true);
  }
  
  const signup = useCallback(async () => {
    let res = await fetch(`${process.env.REACT_APP_SERVER_URL}/api/v1/add-user`, {
      method:'POST',
      headers:{
        'Content-Type': 'application/json',
      }, 
      body: JSON.stringify(values),
    });
    
    let data = await res.json();
    setIsSubmitting(false);
    
    if ( data.status === 200 ) {
      setMessage(data.message);
      setValues({});
    } else if ( data.status === 400 ) {
      setMessage(data.message);
    }
  }, [values]);

  useEffect( () => {
    if (Object.keys(errors).length === 0 && isSubmitting) {
      signup();
    } else {
      setIsSubmitting(false);
    }
  }, [errors, isSubmitting, signup]);

return <>
  <div >
    {message && (<p className="error-style">{message}</p>)}
    <form onSubmit={handleSubmit} id="login-form">
      <div className="row">
        <div className="col s12">

          <div className="row">
            <div className="input-field col m6">
              <label>Name</label>
              <input type="text" name="name" value={values.name || ''} onChange={handleChange}  autoComplete="off" required/>
              {errors.name && ( <p style={errorStyle}>{errors.name}</p>)}
            </div>
            <div className="input-field col m6">
              <label>Email</label>
              <input type="text" name="email" value={values.email || ''} autoComplete="off" onChange={handleChange} required/>
              {errors.email && ( <p style={errorStyle}>{errors.email}</p>)}
            </div>
          </div>

          <div className="row">
            <div className="input-field col m6">
              <label>Password</label>
              <input type="password" name="password" value={values.password || ''} onChange={handleChange} autoComplete="off" required/>
              {errors.password && (<p style={errorStyle}>{errors.password}</p>)}
            </div>
            <div className="input-field col m6">
              <label>Confirm Password</label>
              <input type="password" name="confirm_password" value={values.confirm_password || ''} onChange={handleChange} autoComplete="off" required/>
              {errors.confirm_password && ( <p style={errorStyle}>{errors.confirm_password}</p>)}
            </div>
          </div>

          <div className="row">
            <div className="input-field col m6">
              <label>Phone</label>
              <input type="text" name="phone" value={values.phone || ''} onChange={handleChange} autoComplete="off" required/>
              {errors.phone && ( <p style={errorStyle}>{errors.phone}</p>)}
            </div>
            <div className="input-field col m6">
              <label>Pincode</label>
              <input type="text" name="pincode" value={values.pincode || ''} onChange={handleChange} autoComplete="off" required/>
              {errors.pincode && ( <p style={errorStyle}>{errors.pincode}</p>)}
            </div>
          </div>

        </div>
      </div>
      <button className="btn waves-effect waves-light" type="submit" disabled={!validateForm()}>Submit <i className="material-icons right">send</i> </button>
    </form>
  </div>
  </>;
}

export default AddUser;