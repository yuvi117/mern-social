import React from 'react';
import swal from 'sweetalert';
import { API_URL } from '../../constants/config';
import { NavLink, withRouter } from "react-router-dom";

const Header = (props) => {

  const logout = async () => {

    let res = await fetch(`${API_URL}/logout`, {
      method: 'POST',
      headers :{ 
        'Content-Type': 'application/json',
        Authorization : 'Bearer ' + localStorage.getItem('jwtToken')
      }
    });
    let data = await res.json();

    if (data.status === 200) {
      localStorage.removeItem('jwtToken');
      localStorage.removeItem('userId');
      localStorage.removeItem('userEmail');
      props.history.push("/login");
      swal(data.message);
    } else if (data.status === 401 || data.status === 404) {
      console.log('dd', data.message);
      swal(data.message);
    }
  }

  const renderAuthHeader = () => {
    if (localStorage.getItem('jwtToken')) {
      return  <>
        <li><NavLink to='/inbox'>Inbox</NavLink></li>
        <li><NavLink exact to='/users'>Users</NavLink></li>
        <li><NavLink to='/my-friends'>My friends</NavLink></li>
        <li><NavLink exact to='/my-requests'>Requests</NavLink></li>
        <li><a href="# " onClick={logout}>Logout</a></li>
      </>;
    }
    return ( <li><NavLink to='/login'>Login</NavLink></li>);
  };

  return <>
    <nav>
      <div className="nav-wrapper" style={{background:"#766d82"}}>
      <button className="btn waves-effect waves-light" >{process.env.REACT_APP_NAME} </button>
        <ul id="nav-mobile" className="right">
          <li><NavLink exact to='/'>Dashboard</NavLink></li>
          <li><NavLink exact to='/add-user'>Add User</NavLink></li>
          <li><NavLink exact to='/add-user-fun'>Add User Functional</NavLink></li>
          {renderAuthHeader()}
        </ul>
        </div>
    </nav>
  </>
}

export default withRouter(Header);