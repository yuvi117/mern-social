import React from 'react';

const TableRow = (props) => {

    return <>
        <tr>
            <td>{props.data.id}</td>
            <td>{props.data.name}</td>
            <td>{props.data.age}</td>
        </tr>
    </>
}

export default TableRow;