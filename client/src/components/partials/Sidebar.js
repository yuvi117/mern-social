import React from 'react';
import { NavLink } from "react-router-dom";

const Sidebar = ( ) => {

    return <>
        <NavLink exact to='/employees'>Employees</NavLink>
        <a href="#services">Services</a>
        <a href="#clients">Clients</a>
        <a href="#contact">Contact</a>
    </>
}

export default Sidebar;

