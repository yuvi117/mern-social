import { combineReducers } from 'redux';

import users from './usersReducer';
import employees from './employeesReducer';
import error from './errorReducer';
import success from './successReducer';

const reducers = combineReducers({
  users,
  employees,
  error,
  success,
});

export default reducers;
