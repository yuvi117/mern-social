import { GET_EMPLOYEES } from '../constants/employees';

const employeesReducer = ( state={}, action) => {
    
    switch (action.type) {
        case GET_EMPLOYEES: 
            return action.payload;
        default: return state;
    }
}

export default employeesReducer;