import { GET_EMPLOYEES } from '../constants/employees';
import { GET_ERROR } from '../constants/error';

export const getEmployees = (status = false) => dispatch => {

  if (status === false) {
    return dispatch({
      type: GET_ERROR,
      payload: { msg: 'hello m error message' }
    });
  }

  fetch(`http://dummy.restapiexample.com/api/v1/employees`)
    .then(res => res.json())
    .then(data => {
      if (data.status === 'success'){
        dispatch({ type: GET_EMPLOYEES, payload: data.data });
      }
    })
    .catch(err => {
      console.log('in emp action get emp', err)
      dispatch({type: 'GET_ERROR', payload: { msg: 'm epm api error message' }});
    });
}