import setAuthToken from '../../utils/setAuthToken';
// import jwt_decode from 'jwt-decode';
import { API_URL } from '../../constants/config';

// export const getUsers = () => {
//   return (dispatch) => {
//     fetch(`https://jsonplaceholder.typicode.com/users`)
//     .then(res => res.json())
//     .then(data => {
//         dispatch({type: 'CHANGE_NAME', payload: data});
//     })
//   }
// }

export const getUsers = (data) => dispatch => {
  fetch(`${API_URL}/users`, {
    method:'get',
    headers : setAuthToken(localStorage.getItem('jwtToken')),
    body: JSON.stringify({_id:localStorage.getItem('userId'), ...data})
  })
  .then(res => res.json())
  .then(data => {
    if (data.status === 200){
      dispatch({type: 'GET_USERS', payload: data});
    }
  })
  .catch(err => {
    dispatch({type: 'GET_ERROR', payload: err});
  });
};

// Get User
export const getUser = id => dispatch => {
  fetch(`https://jsonplaceholder.typicode.com/users`)
  .then(res => res.json())
  .then(data => {
      dispatch({type: 'GET_USER', payload: data});
  })
  .catch(err =>
    dispatch({
      type: 'GET_ERROR',
      //payload: err.response.data.message
      payload: err
    })
  );
};

// Set logged in user
export const setCurrentUser = userProfileDetail => {
  return {
    type: 'SET_CURRENT_USER',
    payload: userProfileDetail
  };
};

// Set user auth true
export const setAuthTrue = payload => ({
  type: 'SET_AUTH_TRUE',
  payload
});

// Log user out
export const logoutUser = history => dispatch => {
  // Remove token from localStorage
  localStorage.removeItem('jwtToken');
  // Remove auth header for future requests
  setAuthToken(false);
  // Set current user to {} which will set isAuthenticated to false
  dispatch(setCurrentUser({}));
  // history.push('/login');
};

// Clear error
export const clearError = () => {
  return {
    type: 'CLEAR_ERROR'
  };
};

// Clear success
export const clearSuccess = () => {
  return {
    type: 'CLEAR_SUCCESS'
  };
};

//empty fetched user
export const emptyFetchedUser = () => dispatch =>
  dispatch({
    type: 'EMPTY_FETCHED_USER'
  });
