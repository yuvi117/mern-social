import React, { useEffect, useState, useCallback } from 'react';
// import { Button } from '@material-ui/core';
import io from 'socket.io-client';
import { API_URL, SOCKET_URL } from '../../constants/config';

const moment  = require('moment');
const url = `${SOCKET_URL}?accessToken=${localStorage.getItem('jwtToken')}`;
const socket = io(url);

const Chat = (props) => {

  const [chats, setChats] = useState([]);
  const [error, setError] = useState("");
  const [message, setMessage] = useState("");

  const userData = useCallback(async () => {
    let res = await fetch(`${API_URL}/chat/${props.match.params._id}`,{
      headers :{
        'Content-Type': 'application/json',
        'Authorization' : 'Bearer '+ localStorage.getItem('jwtToken')
      }
    });
    let chats = await res.json();
    // console.log('chat', chats);
    if ( chats.status === 200 ) {
      //setError(chats.message);
      setChats(chats.data);
    } else if ( chats.status === 400 ){
      setError(chats.message);
    }
  }, [props.match.params._id]);

  useEffect( () => {
    userData();  
    socket.on('connect', () => {
      console.log('Im connected')
    });
    socket.on('disconnect', function(){
      console.log('Im disconnected')
    });
    socket.on('messageSent', chat => {
      setChats(chats => [...chats, chat]);
      setMessage("");
    });
    socket.on('messageReceived', chat => {
      setChats(chats => [...chats, chat]);
    });
    let objDiv = document.getElementById("chatList");
    objDiv.scrollTop = objDiv.scrollHeight;
    // console.log('here');
  }, [chats.length, userData]);

  async function handleSubmit(e) {        
    e.preventDefault();
    
    socket.emit('send', {
      message: message,
      receivedBy: props.match.params._id,
      sentBy: localStorage.getItem('userId') 
    });
  }

  return <>
    {error}
    <div className="">
      <div className="row " >
        <div className="col m12">
          <div className="col m8 chatWindow">        
            <ul className="chat" id="chatList">
              {Object.values(chats).map( (chat, i) => 
                <div key={i}>
                  {localStorage.getItem('userId') === chat.sentBy._id ? (
                    <li className="self">
                      <div className="msg">
                        <p>{'you'}</p>
                        <div className="message"> {chat.message}</div>
                        <p>{moment(chat.createdAt).fromNow()}</p>
                      </div>
                    </li>
                  ) : (
                    <li className="other">
                      <div className="msg">
                        <p>{chat.sentBy.name}</p>
                        <div className="message"> {chat.message} </div>
                        <p>{moment(chat.createdAt).fromNow()}</p>
                      </div>
                    </li>
                  )}
                </div>
              )}
            </ul>
            <div className="chatInputWrapper">
              <form onSubmit={handleSubmit} id="chat-form">

              <div className="col m11">
                <input className="textarea input"
                  type="text"
                  value={message}
                  onChange={e => setMessage(e.target.value)}
                  placeholder="Enter your message..."
                />
                </div>
                <div className="col m1">
                <button type="submit" className="chat-button" >Send</button>
                </div>
              </form>
            </div>
          </div>
            <div className="col m4 chatWindow">
              Online list
            </div>
        </div>
      </div>
  </div>
  </>
}

export default Chat;