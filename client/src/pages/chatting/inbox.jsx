import React, { useEffect, useState, useCallback } from 'react';
import { NavLink } from 'react-router-dom';
//import io from 'socket.io-client';
//const moment  = require('moment');

import { getMethod } from '../../utils/methods';

const Inbox = (props) => {

  const [users, setUsers] = useState([]);
  const [error, setError] = useState("");

  const inboxData = useCallback(async () => {
    // let res = await fetch(`${process.env.REACT_APP_SERVER_URL}/api/v1/inbox`,{
    //   headers :{
    //     'Content-Type': 'application/json',
    //     'Authorization' : 'Bearer '+ localStorage.getItem('jwtToken')
    //   }
    // });
    // let users = await res.json();
    // if ( users.status === 200 ) {
    //   // setError(users.message);
    //   setUsers(users.data);
    // } else if ( users.status === 400 ){
    //   setError(users.message);
    // }
  }, []);

  useEffect( () => {
    inboxData();  
    // socket.on('connect', () => {
    //   console.log('Im connected')
    // });
    // socket.on('disconnect', function(){
    //   console.log('Im disconnected')
    // });
    // socket.on('messageSent', chat => {
    //   setChats(chats => [...chats, chat]);
    //   setMessage("");
    // });
    // socket.on('messageReceived', chat => {
    //   setChats(chats => [...chats, chat]);
    // });
    let rr = getMethod(`${process.env.REACT_APP_SERVER_URL}/api/v1/inbox`);
    rr.then((res)=>{
      console.log('res', res);
      setUsers(res.data.data);
    })
    .catch((err) => {
      console.log('er', err);
    });
    // console.log('here', rr);
  }, [users.length, inboxData]);

  return <>
    {error}
    <div className="flexGrow: 1">
      <div className="row">
        <div className="col m12">
          <div className="col m8 chatWindow">
            {Object.values(users).map( (user, i) => 
              <div key={i}>
                {localStorage.getItem('userId') === user.userOne._id ? (                
                  <div className="msg">
                    <p>{user.userTwo.name}</p>
                    <p><NavLink className="btn waves-effect waves-light" to={`/chat/${user.userTwo._id}`} >Chat </NavLink></p>
                  </div>
                ) : (
                  <div className="msg">
                    <p>{user.userOne.name}</p>
                    <div className="message"> <NavLink className="btn waves-effect waves-light" to={`/chat/${user.userOne._id}`} >Chat <i className="material-icons right"></i></NavLink> </div>
                  </div>
                )}
                </div>
              )}
          </div>
            <div className="col m4 chatWindow">
              
            </div>
        </div>
      </div>
  </div>
  </>
}

export default Inbox;