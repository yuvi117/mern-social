import React, { useEffect, useState, useCallback } from 'react';
import { Button, Grid, Paper } from '@material-ui/core';

const Profile = (props) => {

  const [user, setUser] = useState(false);
  const [message, setMessage] = useState(false);

  const userData = useCallback(async () => {  
    let { _id } = props.match.params;

    let res = await fetch(`${process.env.REACT_APP_SERVER_URL}/api/v1/user/${_id}`,
      {
        method:'get',
        headers :{
          'Content-Type': 'application/json',
          'Authorization' : 'Bearer '+ localStorage.getItem('jwtToken')
      }
    });
    let users = await res.json();

    if ( users.status === 200 ) {
      setUser(users.data);
    } else if ( users.status === 400 ){
      setMessage(users.message);
    }
  }, [props.match.params]);

  useEffect( () => {
    userData();  
    console.log('in profile');
  }, [user.length, userData]);

  function handleRequest(id) {
    props.history.push(`/chat/${id}`);
  }

  return <>
  <div className="flexGrow: 1">
    {message}
    <Grid container spacing={3}>
      <Grid item xs={1}></Grid>
      <Grid item xs={3}>
        <Paper >Name</Paper>
      </Grid>
      <Grid item xs={3}>
        <Paper >{user.name}</Paper>
      </Grid>
      <Grid item xs={1}>
        <Button variant="contained" color="secondary" onClick={() => handleRequest(user._id)} >Chating</Button>
      </Grid>
    </Grid>
    <Grid container spacing={3}>
      <Grid item xs={1}></Grid>
      <Grid item xs={3}>
        <Paper >Email</Paper>
      </Grid>
      <Grid item xs={3}>
        <Paper >{user.email}</Paper>
      </Grid>
    </Grid>
    <Grid container spacing={3}>
      <Grid item xs={1}></Grid>
      <Grid item xs={3}>
          <Paper >Phone</Paper>
      </Grid>
      <Grid item xs={3}>
          <Paper >{user.phone}</Paper>
      </Grid>
    </Grid>
  </div>
  </>
}

export default Profile;