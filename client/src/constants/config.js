export const SERVER_URL= 'http://localhost:4000';
export const API_URL = 'http://localhost:4000/api/v1';
export const SOCKET_URL = 'http://localhost:8002';
export const HEADER = {
    'Content-Type': 'application/json',
    'Authorization':'Bearer ' + localStorage.getItem('jwtToken')
}