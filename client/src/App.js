import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Layouts from './components/layouts/layouts.component';
import Header from './components/header/header.component';
import Sidebar from './components/partials/Sidebar';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'devextreme/dist/css/dx.light.css';
import 'devextreme/dist/css/dx.common.css';
import './App.css';

function App() {
  return (<>
    <BrowserRouter>
      <div className="wrapper">
        <div className="static">
          <div className="header">
            <Header/>
          </div>   
          {/* <div id="sidebar"> */}
            <div className="sidebar">
              <Sidebar />
            </div>
          {/* </div> */}
        </div>
        <div className="content">
          <Layouts/>
        </div>
      </div>
    </BrowserRouter>
  </>);
}

export default App;
