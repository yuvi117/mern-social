// import axios from 'axios';

const setAuthToken = token => {
  const headers = {
    Accept: "application/json",
    "Content-Type": "application/json"
  };
  if (token) {
    // Apply to every request
   // axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    // Setting Authorization header
    // Authorization: Bearer xxxxxxx.xxxxxxxx.xxxxxx
    //if (this.loggedIn()) {
      headers["Authorization"] = "Bearer " + token;
      return headers;
    //}
  } else {
    // Delete auth header
    delete headers['Authorization'];
  }
};

export default setAuthToken;
