import React from 'react';
import axios from 'axios';

axios.interceptors.request.use(
  async config => {
    config.headers = { 
      'Authorization': `Bearer ${localStorage.getItem('jwtToken')}`,
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
    return config;
  },
  error => {
    Promise.reject(error)
});

axios.interceptors.response.use((response) => { return response}, (error) => {
  
  const originalRequest = error.config;
  const status = error.response ? error.response.status : null;
  console.log('my status', status);

  if (!status) {
    console.log('Backend not started yet!')
  }
  let refreshToken = localStorage.getItem('refreshToken');

  if (
    refreshToken &&
    error.response.status === 401 &&
    !originalRequest._retry
    ) {
      console.log('Backend not started yet!')
    originalRequest._retry = true;
    return axios
      .post(`http://localhost:4000/api/v1/auth/refresh_token`, {refreshToken: refreshToken}, {headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded'
      }})
      .then((res) => {
        if (res.status === 200) {
          // console.log("Access token refreshed!", res.data.accessToken);
          localStorage.setItem("jwtToken", res.data.accessToken);
          return axios(originalRequest);
        }
      });
  }
  return Promise.reject(error);
});

export function getMethod(url, data) {
   return axios(url);
 }
 
 export function postMethod(url, data) {
   return true;
 }
 
 export function updateMethod(url, data) {
   return true;
 }
 
 export function deleteMethod(url, data) {
   return true;
 }